import React, { Component } from 'react'
import style from './form.module.scss';

class Form extends Component {

    constructor(props) {
        super(props);   
        this.state = {
            first_name: ''
        }
    }

    onChange = event => {
        this.setState({
            first_name: event.target.value
        })
    }

    onSubmit = event => {
        event.preventDefault();
        alert(`Bonjour ${this.state.first_name} !`)
    }

    render() {
        return (
            <div>
                <form className={style.box} onSubmit={this.onSubmit}>
                    <h1>Votre pr&eacute;nom</h1>
                    <input type="text" placeholder="Pr&eacute;nom" onChange={this.onChange} required />
                    <input type="submit" value="Clique sur moi" />
                </form>
            </div>
        )
    }
}

export default Form
