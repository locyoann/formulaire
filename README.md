# Basic Form in React
Display an alert when you submit the form.
This alert displays 'hello ...' with '...' replaced by what you write in the input.


## Installation
* `npm install`
* `npm start`

## Contributors
* Yoann Paquereau (Form + Alert)
* Loïc Thibault (style)